# tuntsCorp Test

A simple application using node.js to read and update a Google Spreadsheet.

The spreadsheet contains a list of students and their grades. It must be updated according to these rules:
* If the final grade is lower than 50, student is failed by grade.
* If the final grade is between 50 and 70, the student must attend to the final exam.
* * In this case, the required grade for approval (rga) must be calculated, following the formula: 5 <= (m + rga)/2
* If the grade is above 70, the student is approved.
* If the number of absences is higher than 25% of the total ammount of classes (which is also in the Spreadsheet), the student is failed by excessive absences, regardless of the final grade. 

Link to the [spreadsheet](https://docs.google.com/spreadsheets/d/1neDtpNWjSY1bR0ZeNNOwM63KxSCOImURnBNpj3O8wEM/edit#gid=0).

## Getting Started

Simply using 'node . ' inside the directory with these files will run the application. For the first time its run, you will need to access a link shown in console in order to obtain a token, that will be used to access the spreadsheet.

### Prerequisites
 
To run this application, you must have [node.js](https://nodejs.org/en/download/) installed on your computer. 

