const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const spreadsheetId = '1neDtpNWjSY1bR0ZeNNOwM63KxSCOImURnBNpj3O8wEM';

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

console.log("Abrindo planilha...");

// Load client secrets from a local file.
fs.readFile('credentials.json', (err, content) => {
  if (err) return console.log('Erro ao carregar o arqivo de segredo: ', err);
  // Authorize a client with credentials, then call the Google Sheets API.
  authorize(JSON.parse(content), calculateResultsAndGrades);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Autorize essa aplicação acessando este link:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Digite o código obtido aqui: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Erro ao tentar obter token de acesso: ', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token salvo em ', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

//Calculate students grades,and show them on the console.
//Sets an array containing the situations and the required grades.
function calculateResultsAndGrades(auth){
  let result = [];
  let totalHoras;
  const sheets = google.sheets({version: 'v4', auth});

  //Gets the cell where the total ammount of hours is written.
  //Assuming the spreadsheet would respect a pattern, this allows this function to
  //calculate the % of absences regardless of the total hours. 
  console.log("Buscando total de horas da disciplina...");
  sheets.spreadsheets.values.get({
    spreadsheetId: spreadsheetId,
    range: 'A2'
  }, (err, res) => {
    if (err) console.log("A API retornou um erro: ", err);
    var row = res.data.values;
    row.map((cell) =>{
      totalHoras = parseInt(cell[0].match(/\d+/g));
    });
    //Now, retrieve the students' names, absences and grades, in order to calculate the results.
    sheets.spreadsheets.values.get({
      spreadsheetId: `${spreadsheetId}`,
      range: 'B4:F'
    }, (err, res) => {
      if (err) return console.log('A API retornou um erro: ', err);
      const rows = res.data.values;
      if (rows.length) {
        console.log("Iniciando cálculos... ");
        rows.map((row) => {
          let temp = [];
          if (`${row[1]}` > totalHoras/4) { 
            console.log(`${row[0]} tem muitas faltas e está reprovado!`);
            temp[0] = "Reprovado por falta!";
            temp[1] = 0;
            result.push(temp);
          }
          else {
            var media = (parseFloat(row[2]) + parseFloat(row[3]) + parseFloat(row[4])) / 3;
            console.log(`${row[0]} ficou com media `, media.toPrecision(3), `!`);
            if (media < 50){
              temp[0] = "Reprovado por media!";
              temp[1] = 0;
              result.push(temp);
            }
            else if (media < 70){
              temp[0] = "Exame Final!";
              temp[1] = Math.ceil(100-media);
              result.push(temp);
            }
            else{
              temp[0] = "Aprovado!";
              temp[1] = 0;
              result.push(temp);
            }
          }
        });
        updateSpreadsheet(auth, result);
      }
    });
  });
}

function updateSpreadsheet(auth, result){
  console.log("Começando atualização...");
  const sheets = google.sheets({version: 'v4', auth});
  sheets.spreadsheets.values.update({
    'spreadsheetId': `${spreadsheetId}`,
    'range': 'G4',
    'valueInputOption': 'RAW',
    'resource': {
      'values':  result
    },
    'auth': auth
  }, (err, result) => {
    if (err) console.log("Erro: ", err);
    else console.log('Planilha atualizada!');
  });
}